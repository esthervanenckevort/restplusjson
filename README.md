# RESTplusJSON

[![CI Status](https://img.shields.io/travis/David van Enckevort/RESTplusJSON.svg?style=flat)](https://travis-ci.org/David van Enckevort/RESTplusJSON)
[![Version](https://img.shields.io/cocoapods/v/RESTplusJSON.svg?style=flat)](https://cocoapods.org/pods/RESTplusJSON)
[![License](https://img.shields.io/cocoapods/l/RESTplusJSON.svg?style=flat)](https://cocoapods.org/pods/RESTplusJSON)
[![Platform](https://img.shields.io/cocoapods/p/RESTplusJSON.svg?style=flat)](https://cocoapods.org/pods/RESTplusJSON)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RESTplusJSON is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'RESTplusJSON'
```

## Author

David van Enckevort, david@allthingsdigital.nl

## License

RESTplusJSON is available under the MIT license. See the LICENSE file for more info.
